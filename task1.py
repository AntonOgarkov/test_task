#!/usr/bin/env python3
def random_seq(lst, k):
    '''This function chooses k unique random values from list
    input list and int
    output set'''
    if isinstance(lst, list) and isinstance(k, int):
        if len(lst) >= k:
            from random import shuffle
            shuffle(lst)
            res_set = set()
            while len(res_set) != k:
                res_set.add(lst.pop())
            return res_set
        else:
            raise ValueError
    else:
        raise TypeError