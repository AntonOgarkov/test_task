import bisect
import datetime


class GetStat():
    '''Класс вычисляет среднее, медиану, максимальное, минимальное значение, затем их групирует'''
    def __init__(self):
        self.mean = 0
        self.max = 0
        self.min = 0
        self.median = 0
        self.last = [0]
        self.sorted = []
        self.array = []
        self.grouped = []
        self.shift = 0

    def get_array(self, arr):
        "получение массива данных"
        arr = [list(i) for i in arr]
        self.array = arr
        self.shift = int((arr[0][0] - arr[0][0].replace(hour=0, minute=0, second=0)).total_seconds())+1

    def group(self):
        "группировка обработанных данных"
        self.grouped = [self.array[:self.shift]]
        l = list(self.array[self.shift+i:self.shift+i + 86400] for i in range(0, len(self.array)-self.shift, 86400))
        self.grouped.extend(l)

    def next_mean(self, first):
        "среднее на основе прошлого значения"
        self.mean = self.mean + (first - self.last[0])/100

    def next_sorted(self, first):
        "обновление сотни отсортированных значений"
        self.sorted.remove(self.last[0])
        bisect.insort(self.sorted, first)

    def next_max(self):
        "максимальное на основе прошлого значения"
        self.max = self.sorted[-1]

    def next_min(self):
        "минимальное на основе прошлого значения"
        self.min = self.sorted[0]

    def next_median(self):
        "медиана на основе прошлых значений"
        "работает на основе отсортированного значения"
        self.median = sum(self.sorted[49:51]) / 2

    def set_first100(self, arr):
        "расчет для первых ста значений"
        self.last = list(list(zip(*arr))[1])
        self.sorted = self.last.copy()
        self.sorted.sort(reverse=False)
        self.mean = sum(self.sorted)/100
        self.max = max(arr)[1]
        self.min = min(arr)[1]
        self.next_median()
        self.array[100].extend([self.mean, self.max, self.min, self.median])

    def iter(self):
        "основной блок, в котором расчитываются статистические данные"
        self.array[0] = list(self.array[0])
        self.set_first100(self.array[1:101])
        self.array.insert(0,['timestamp','value','mean', 'min', 'max', 'median'])
        for i in self.array[102:]:
            self.next_mean(i[1])
            self.next_sorted(i[1])
            self.next_max()
            self.next_min()
            self.next_median()
            self.last.reverse()
            self.last.pop()
            self.last.reverse()
            self.last.append(i[1])
            i.extend([self.mean, self.min, self.max, self.median])
        self.group()
