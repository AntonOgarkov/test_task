#!/usr/bin/env python3
from task1 import random_seq
import sys
import unittest


class TestRandomSeq(unittest.TestCase):

    def test_calculation(self):
        self.assertEqual({1, 2, 3, 4, 5, 6}, random_seq([1, 2, 3, 4, 5, 6], 6))

    def test_negative(self):
        self.assertRaises(ValueError, random_seq, [1, 2, 3, 4, 5, 6], 10)

    def test_type(self):
        self.assertRaises(TypeError, random_seq, 213, '3')

    def test_zero(self):
        self.assertEqual(set(), random_seq([], 0))