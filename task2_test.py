#!/usr/bin/env python3
from task2 import GetStat
import sys
import unittest
import datetime
now = datetime.datetime.now()
sample = list(zip(list(now - datetime.timedelta(seconds=i) for i in range(5*24*60*60)), list(range(5*24*60*60))))

class TestRandomSeq(unittest.TestCase):

    def test_next_mean(self):
        obj = GetStat()
        obj.array = list(range(100))
        obj.mean = sum(range(100))/100
        obj.next_mean(100)
        self.assertEqual(50.5,obj.mean)

    def test_next_max(self):
        obj = GetStat()
        obj.sorted = list(range(100))
        obj.next_max()
        self.assertEqual(99,obj.max)

    def test_next_min(self):
        obj = GetStat()
        obj.sorted = list(range(10,110))
        obj.next_min()
        self.assertEqual(10, obj.min)

    def test_next_median(self):
        obj = GetStat()
        obj.sorted = list(range(100))
        obj.next_median()
        self.assertEqual(49.5, obj.median)


    def test_get_array(self):
        obj = GetStat()
        obj.get_array(sample.copy())


    def test_iter(self):
        obj = GetStat()
        obj.get_array(sample.copy())
        obj.iter()
        self.assertEqual(50.5, obj.grouped[0][101][-1])
        self.assertEqual(100, obj.grouped[0][101][4])
        print(obj.grouped[1][0])
        print(obj.grouped[1][-1])
        # self.assertEqual(0,(obj.grouped[1][0][0]-obj.grouped[1][0][0].replace(hour=0, minute=0, second=0)).total_seconds())
        # self.assertEqual(1,(obj.grouped[1][-1][0]-obj.grouped[1][-1][0].replace(hour=0, minute=0, second=0)).total_seconds())
